FROM python:3.9-slim

ARG PORT=80
ENV PORT $PORT
ARG HOST=0.0.0.0
ENV HOST $HOST

ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY . .

RUN apt-get update \
  && apt-get install -y --no-install-recommends \ 
  make libpq-dev \
  python3-dev \
  build-essential \
  && rm -rf /var/lib/apt/lists/* \
  && apt clean

RUN make install

EXPOSE $PORT
# Run the web service on container startup. Here we use the gunicorn
# webserver, with one worker process and 8 threads.
# For environments with multiple CPU cores, increase the number of workers
# to be equal to the cores available.
CMD exec gunicorn --bind $HOST:$PORT --workers 1 --threads 8 --timeout 0 pharma.wsgi:application