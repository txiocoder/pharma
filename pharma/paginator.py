from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from typing import (
  Tuple
)

class InitLimitPaginatorMixin(object):

  pg_response_class = Response
  pg_exception_class = ValidationError

  pg_min_limit: int = 1
  pg_max_limit: int = 5000
  pg_limit_query_param: str = 'limit'
  pg_init_query_param: str = 'init'
  pg_order_query_param: str = 'order_by'
  pg_entries_key: str = 'results'

  pg_init: int = 0
  pg_limit: int = 100
  pg_order_by: str = '-pk'
  pg_order_choices: Tuple[str] = (
    '-pk',
    'pk',
  )

  def paginate (self, queryset):
    self.__set_defaults()
    self.pg_count = queryset.count()

    if self.pg_count == 0 or self.pg_init > self.pg_count:
      return list()

    return queryset.order_by(
      self.pg_order_by)[self.pg_init:self.pg_init + self.pg_limit]
  
  def get_paginated_response (self, queryset,
    serializer_class=None, status_code=200):
    """
    Paginate a Queryset and Returns a rest_framework.response.Response
    (by default) with queryseta data serialize and total objects count
    """
    
    queryset = self.paginate(queryset)
    serializer_class = serializer_class or self.serializer_class
    assert serializer_class is not None

    data = serializer_class(queryset, many=True,
      context={'request': self.request}).data

    return Response({
      'count': self.pg_count,
      'next': self.__get_next(),
      'previous': self.__get_previous(),
      self.pg_entries_key: data
      }, status=status_code)
  
  def __get_next (self):
    init = None
    if (next_pg := self.pg_limit + self.pg_init) < self.pg_count:
      init = next_pg
    
    if init is not None:
      return {
        self.pg_init_query_param: init,
        self.pg_limit_query_param: self.pg_limit
      }
    return None

  def __get_previous (self):
    init = None
    if (previous := self.pg_init - self.pg_limit) >= 0:
      init = previous
    
    if init is not None:
      return {
        self.pg_init_query_param: init,
        self.pg_limit_query_param: self.pg_limit
      }
    return None

  def __set_defaults (self):
    """
    set dafaults pagination 
    values from query paramenters
    """
    self.__set_pg_init()
    self.__set_pg_limit()
    self.__set_pg_order()

  def __set_pg_order (self):
    self.pg_order_by = self.request.GET.get(
      self.pg_order_query_param,
      self.pg_order_by
    )

    if self.pg_order_by not in self.pg_order_choices:
      raise self.pg_exception_class(dict(
        detail=_(
          "the value '%s' of '%s' parameter not in order choices"
        ) % (self.pg_order_by, self.pg_order_query_param)
      ))

  def __set_pg_limit (self):
    try:
      self.pg_limit = int(self.request.GET.get(
        self.pg_limit_query_param,
        self.pg_limit
      ))
    except: pass

    if self.pg_limit < self.pg_min_limit or self.pg_limit > self.pg_max_limit:
      raise self.pg_exception_class(dict(
        detail=_(
          "'%s' parameter must be an integer between %s and %s"
        ) % (self.pg_limit_query_param, self.pg_min_limit, self.pg_max_limit)
      ))

  def __set_pg_init (self):
    try:
      self.pg_init = int(self.request.GET.get(
        self.pg_init_query_param,
        self.pg_init
      ))
    except:
      self.pg_init = 0

    if self.pg_init < 0:
      raise self.pg_exception_class(dict(
        detail=_(
          "'%s' parameter must be "
          "an integer starting form 0"
        ) % self.pg_init_query_param
      ))