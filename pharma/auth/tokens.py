from rest_framework_simplejwt.tokens import RefreshToken

def get_user_token_access_and_refresh (user) -> dict:
  """
  Returns authentication tokens for a user
  using simple jwt
  """
  refresh = RefreshToken.for_user(user)
  return dict(
    access= str(refresh.access_token),
    refresh= str(refresh))