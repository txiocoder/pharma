from django.urls import path
from . import views

app_name = 'auth'

urlpatterns = [
  path('/access', views.access, name='access'),
  path('/refresh', views.refresh.as_view(), name='refresh')
]