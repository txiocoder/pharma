# rest framework
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework_simplejwt.views import TokenRefreshView

from pharma.auth.tokens import get_user_token_access_and_refresh
from django.contrib.auth import get_user_model

@api_view(['POST'])
def access(request):
  """
  Get Token Acess from a random User
  """

  User = get_user_model()

  if (user := User.objects.some()):
    return Response(
      get_user_token_access_and_refresh(user))
  return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)

refresh = TokenRefreshView  