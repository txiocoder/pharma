from django.test import SimpleTestCase
from django.urls import resolve, reverse

from pharma.auth import views

class TestUrls(SimpleTestCase):

  def test_url_auth_access(self) -> None:
    """it should resolve auth:access url name to /auth/access"""
    url = reverse('auth:access')
    self.assertEqual(url, '/auth/access')
    self.assertEquals(
      resolve(url).func,
      views.access
    )
  
  def test_url_auth_refresh(self) -> None:
    """it should resolve auth:refresh url name to /auth/refresh"""
    url = reverse('auth:refresh')
    self.assertEqual(url, '/auth/refresh')
    self.assertEquals(
      resolve(url).func.view_class,
      views.refresh
    )