from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model

from pharma.auth.tokens import (
  get_user_token_access_and_refresh
)

User = get_user_model()

class TestViews(TestCase):

  def setUp(self):
    self.user = User.objects.create(
      name='Yolo Woza',
      username='wozayolo',
      email='a@a.xom')

    self.tokens = get_user_token_access_and_refresh(
      self.user)

    self.client = Client(
      HTTP_AUTHORIZATION=f"Bearer {self.tokens.get('access')}")
  
  def test_view_auth_access(self) -> None:
    """it should return token access and refresh"""
    url = reverse('auth:access')
    r = self.client.post(url)
    self.assertEqual(r.status_code, 200)
    self.assertIsNotNone(r.json().get('access'))
  
  def test_view_auth_refresh(self) -> None:
    """it should refresh the token access"""
    url = reverse('auth:refresh')
    r = self.client.post(url, {'refresh': self.tokens.get('refresh')},
      content_type='application/json')
    self.assertEqual(r.status_code, 200)
    self.assertIsNotNone(r.json().get('access'))
