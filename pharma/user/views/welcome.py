from django.http import response
from rest_framework.response import Response
from rest_framework.decorators import api_view

@api_view(['GET'])
def welcome (request) -> Response:
  return Response('REST Back-end Challenge 20201209 Running')