from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response

from pharma.user.models import User
from pharma.user.serializer import UserSerializer
from pharma.paginator import InitLimitPaginatorMixin

"""
Users API View
"""
class UserAPIView(APIView, InitLimitPaginatorMixin):

  serializer_class = UserSerializer

  pg_order_by = '-date_joined'
  pg_order_choices = (
    'date_joined',
    '-date_joined',
  )

  def get(self, request: Request) -> Response:
    """Get All Registered Users"""
    return self.get_paginated_response(
      User.objects.all()
    )
