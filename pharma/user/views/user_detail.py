from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework import status

from django.shortcuts import get_object_or_404
from pharma.user.models import User
from pharma.user.serializer import UserSerializer

class UserDetailAPIView(APIView):

  serializer_class = UserSerializer
  permission_classes = (
    IsAuthenticatedOrReadOnly,
  )

  def get (self, request, pk: str):
    user = self.get_object(pk)
    return Response(self.serializer_class(user).data)
  
  def put (self, request, pk: str):
    user = self.get_object(pk)
    serializer = self.serializer_class(
      instance=user, data=request.data, partial=True)
    if serializer.is_valid(raise_exception=True):
      serializer.save()
    return Response(serializer.data, status=status.HTTP_202_ACCEPTED) 
  
  def delete (self, request, pk: str):
    user = self.get_object(pk)
    user.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

  def get_object(self, pk):
    user = get_object_or_404(User, pk=pk)
    self.check_object_permissions(self.request, user)
    return user
