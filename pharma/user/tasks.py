import requests
from typing import List

from django.utils.timezone import now
from pharma.user.models import User

def get_calculated_results_list (
  max_results: int, max_results_per_page: int) -> List[int]:
  """
  Calculates the number of each entry for the random api
  Becouse ramdom user api do not have a really pagination
  if max entries is 100
  and per page is set to 80, then this should return:
  [80,20]
  """
  if max_results >= max_results_per_page:
    entries: List[int] = []
    while max_results > 0:
      max_results -= max_results_per_page
      entries.append(
        max_results_per_page if max_results >= 0 else abs(max_results + max_results_per_page))
    return entries
  return list()

def import_users_from_random_user_api(
  max_results: int = 3000,
  max_results_per_page: int = 1000):

  """
  Import ramdom users from random
  user api and save to local model
  """

  max_results = 5000 if max_results > 500 else max_results
  max_results_per_page  = 2000 if max_results_per_page > 2000 else max_results_per_page

  for page, entries in enumerate(
    get_calculated_results_list(max_results, max_results_per_page)):
    with requests.get(
      url='https://randomuser.me/api/',
      params=dict(
        results=entries,
        page=page+1)
      ) as r:
      if r.status_code == 200:
        accounts: List[User] = []
        data: dict = r.json()
        for account in data.get('results', []):
          accounts.append(User(**dict(
            status=User.Status.PUBLISHED,
            type=User.Type.IMPORTED,
            imported_t=now(),
            name=f"{account.get('name').get('first')} {account.get('name').get('last')}",
            username=account.get('login').get('username'),
            email=account.get('email'),
            data=account
          )))
        User.objects.bulk_create(accounts, ignore_conflicts=True)
