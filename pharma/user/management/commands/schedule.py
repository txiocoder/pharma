from django_rq import get_scheduler

from django.core.management.base import BaseCommand
from pharma.user.tasks import (
  import_users_from_random_user_api
)

class Command(BaseCommand):

  help: str = 'Schedule cron job to import users'
  cron_string: str = '0 17 * * *'

  def handle(self, *args, **kwargs):
    scheduler = get_scheduler('default')

    if not scheduler.count():
      job = scheduler.cron(
        self.cron_string,
        kwargs=dict(
          max_results=2800,
          max_results_per_page=500
        ),
        func=import_users_from_random_user_api)
      return self.stdout.write(
        self.style.SUCCESS(f'job <{job}> scheduled!'))
    self.stdout.write(
      self.style.WARNING('Job already scheduled'))