from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model

from pharma.auth.tokens import (
  get_user_token_access_and_refresh
)

User = get_user_model()
class TestViews(TestCase):

  def setUp(self):
    self.user = User.objects.create(
      name='Yolo Woza',
      username='wozayolo',
      email='a@a.xom')

    token_access = get_user_token_access_and_refresh(
      self.user).get('access')

    self.url_user_detail = reverse('users:user_detail', kwargs={
      'pk': self.user.id})

    self.client = Client(
      HTTP_AUTHORIZATION=f'Bearer {token_access}')

  def test_view_users_welcome(self) -> None:
    """it should return welcome message"""
    url = reverse('welcome')
    r = self.client.get(url)
    self.assertEqual(r.status_code, 200)
    self.assertEqual(r.json(), 'REST Back-end Challenge 20201209 Running')

  def test_view_users_users(self) -> None:
    """it should return 200 status code"""
    url = reverse('users:users')
    r = self.client.get(url)
    self.assertEqual(r.status_code, 200)
  
  def test_view_users_user_detail_GET(self) -> None:
    """it should get user detail"""
    r = self.client.get(self.url_user_detail)
    self.assertEqual(r.status_code, 200)
    self.assertEquals(r.json().get('id'), self.user.id.__str__())
  
  def test_view_users_user_detail_DELETE(self) -> None:
    """it should delete a user"""
    r = self.client.delete(self.url_user_detail )
    self.assertEqual(r.status_code, 204)
  
  def test_view_users_user_detail_PUT(self) -> None:
    """it should update user"""
    name = 'Wozaa'
    r = self.client.put(self.url_user_detail, {'name': name},
      content_type='application/json')
    self.assertEqual(r.status_code, 202)
    self.assertEqual(r.json().get('name'), name)
