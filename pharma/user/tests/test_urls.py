import uuid
from django.test import SimpleTestCase
from django.urls import resolve, reverse

from pharma.user import views

class TestUrls(SimpleTestCase):

  def setUp(self) -> None:
    self.uuid_string = uuid.uuid4()

  def test_url_users_welcome(self) -> None:
    """it should resolve users:welcome url name to /"""
    url = reverse('welcome')
    self.assertEqual(url, '/')
    self.assertEqual(
      resolve(url).func,
      views.welcome
    )
  
  def test_url_users_users(self) -> None:
    """it should resolve users:users url name to /users"""
    url = reverse('users:users')
    self.assertEqual(url, '/users')
    self.assertEquals(
      resolve(url).func.view_class,
      views.UserAPIView
    )
  
  def test_url_users_user_detail(self) -> None:
    """it should resolve users:user_detail url name to /users/<uuid>/"""
    url = reverse('users:user_detail', kwargs=dict(
      pk=self.uuid_string))
    self.assertEqual(url, f'/users/{self.uuid_string}')
    self.assertEquals(
      resolve(url).func.view_class,
      views.UserDetailAPIView
    )