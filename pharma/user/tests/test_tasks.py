from django.test import TestCase
from pharma.user.models import User
from pharma.user.tasks import (
  import_users_from_random_user_api
)

class TestViews(TestCase):

  def test_task_import_users_from_random_api(self) -> None:
    """it should add more users"""
    import_users_from_random_user_api(3, 2)
    self.assertGreaterEqual(User.objects.count(), 3)
