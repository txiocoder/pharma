import uuid
import random
from django.db import models

from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import (
  AbstractUser, UserManager as UM
)

class UserManager(UM):

  def some(self):
    """Get a rondom user"""
    if (count := self.count()) > 0:
      return self.all()[random.randint(1, count) - 1]
    return None

class UserStatusChoices(models.TextChoices):
  DRAFT     = 'draft', _('draft')
  TRASH     = 'trash', _('trash')
  PUBLISHED = 'published', _('published')

class UserTypeChoices(models.TextChoices):
  NATIVE = 'native', _('native account')
  IMPORTED = 'imported', _('imported account')

class User(AbstractUser):

  class Meta:
    verbose_name = _('user')
    verbose_name_plural = _('users')

  Status = UserStatusChoices
  Type = UserTypeChoices

  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
  name = models.CharField(_('name'), max_length=120, blank=True)
  data = models.JSONField(_('imported data'), default=dict)
  imported_t = models.DateTimeField(_('imported at'), null=True, default=None)
  
  status = models.CharField(
    _('status'),
    max_length=10,
    choices=Status.choices,
    default=Status.PUBLISHED
  )
  type = models.CharField(
    _('account type'),
    max_length=10,
    choices=Type.choices,
    default=Type.NATIVE
  )
  
  objects = UserManager()
  first_name = None
  last_name = None