from rest_framework import serializers
from pharma.user.models import User

class UserSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = [
      'id', 'name', 'username', 'email', 'imported_t', 'status',
      'type', 'data'
    ]
  
  id = serializers.CharField(read_only=True)
  imported_t = serializers.DateTimeField(read_only=True)
  status = serializers.ChoiceField(read_only=True, choices=User.Status.choices)
  type = serializers.ChoiceField(read_only=True,choices=User.Type.choices)