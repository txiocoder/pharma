from django.urls import path
from pharma.user.views import (
  UserAPIView,
  UserDetailAPIView
)

app_name = 'users'

urlpatterns = [
  path('', UserAPIView.as_view(), name='users'),
  path('/<uuid:pk>', UserDetailAPIView.as_view(), name='user_detail')
]