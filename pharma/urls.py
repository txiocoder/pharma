from django.urls import path, include
from pharma.user.views import welcome

urlpatterns = [
  path('', welcome, name='welcome'),
  path('users', include('pharma.user.urls')),
  path('auth', include('pharma.auth.urls'))
]
