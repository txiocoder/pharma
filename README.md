# Pharma API

> 🏅 Este projecto é a resolução do desafio Backend Developer 2021 da [Coodesh](https://coodesh.com)

A Pharma API é um serviço que colecta periodicamente vários usuarios da api do [Random User Generator](https://randomuser.me/). Tal como proposto no desafio.
Esta API está sendo desenvolvida com Python e Django REST Framework, mais detalhes sobre a stack completo logo abaixo. Para mais detalhes sobre o desafio acesse `./challenge-info/README.md`

Há uma versão deste projecto rodando no Heroku e podes acessar [POR AQUI](https://pharma-am.herokuapp.com/). A documentação dos endpoint já disponíveis estão no [Stoplight](https://stoplight.io/) e pode ser acessado [POR AQUI](https://txiocoder.stoplight.io/docs/am/YXBpOjIwODQwMDg3-pharma)


### Tecnologias

Este projecto esta sendo desenvolvido com [Python](https://www.python.org/), [Django](https://www.djangoproject.com/) e [REST Framework](https://www.django-rest-framework.org/). Gitlab CI/CD para automação (contrução do container docker, testes automatizados e deploy) e rodando no [Heroku](https://heroku.com). Abaixo a lista completa da stack principal e algumas biblitecas que se destacam.

- Pyhon 3.9
- Django 3.2
- REST Framework
- Docker
- Django Redis Queue & Scheduler
- Docker Compose para desenvolvimento local
- PostgreSQL
- Redis

## Rodando com Docker

Caso tenhas o Docker e Docker Compose instalados na sua maquina poderás rodar todo um ambiante para aplicação localmente com um úncio simples comando. Seguindo apenas doas etapas.

1. **Configuração**

Configure inicialmente as variaveis de ambiente necessárias para a construção dos serviços e para rodar os mesmos.
Para facilitar, copie as variaveis definidas no arquivo `./env.example`, crie um arquivo na raiz do projecto de nome `./.env` e cole nele as informações.

Não te esqueças do `SECRET_KEY`. Use o comando abaixo no seu terminal caso precises gerar uma chave aleartoria.

```bash
$ python -c 'import random; result = "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)]); print(result)'
```

1. **Rode**

Por fim, use o comando abaixo para iniciar a construção dos serviços (PostgreSQL, Redis, Pharma API, worker e scheduler) e rodar todos eles. A API terá a porta `8000` exposta e poderás acessar localmente via [https://localhost:8000](https://localhost:8000). Acesse a [documentação da API](https://txiocoder.stoplight.io/docs/am/YXBpOjIwODQwMDg3-pharma) para poderes interagir com a API.

```bash
$ make up
```

Use o comando `make down` ou `docker-compose down` para terminar os servçõs e remover os container.

> Caso desejes rodar outra vez, usa o comando: `docker-compose up` para não ter de reconstruir as imagens docker

## Rodando Localmente

**DICA:** Caso queiras apenas testar e não queres instalar todas as dependencias localmente, nem mesmo o docker. Use o [GitPod.io](https://gitpod.io/workspaces) e siga as instruções acima sobre como rodar com o Docker. O Gitpod já se encontra integrado com o Gitlab, apenas click no botão (`Gitpo`, caso esteja `Web IDE` altere para Gitpo) acima deste repositorio para iniciar um novo workspace.

É isso ai!