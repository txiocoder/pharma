###
## Vars
#

host := 127.0.0.1
port := 8000
user :=
ref  :=

up:
	# up services in -d mode
	@docker-compose up -d --build
	# syncing database
	@docker-compose run api make dbsync
	# scheduling jobs
	@docker-compose run api ./manage.py schedule
	@docker-compose logs -f

down:
	@docker-compose down

run-rqworker:
	# running django RQ worker
	@python manage.py rqworker default

run-rqscheduler:
	# running django RQ worker
	@python manage.py rqscheduler

shedule:
	# scheduling tasks
	@python manage.py schedule

install:
	@pip install -r requirements.txt

serve:
	@python manage.py runserver ${host}:${port}

migrations:
	@python manage.py makemigrations

migrate:
	@python manage.py migrate

shell:
	@python manage.py shell

check:
	@python manage.py check

createsuperuser:
	# creating super user
	@python manage.py createsuperuser ${user}

dbsync:
	# applying migrations
	@python manage.py makemigrations
	# migrate
	@python manage.py migrate

release:
	# applying migrations
	@python manage.py makemigrations
	# migrate
	@python manage.py migrate
	# scheduling tasks
	@python manage.py schedule

test:
	# ---------------------- Testing All Apllication ---------------------
	@python manage.py test \
		--pattern="test_*.py" \
		--keepdb --verbosity 2 \
		--failfast \
		--force-color 

messages:
	# making messages for translations
	@python manage.py makemessages -l pt

compilemessages:
	@django-admin compilemessages
